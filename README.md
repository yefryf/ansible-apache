Role migrated here https://gitlab.devoping.com/devops/ansible-playbooks/-/tree/master/roles/install-apache-v2

# Ansible-Apache

An Ansible role that install and configure Apache2.x on Debian/Ubuntu and CentOS. 

When you run the playbook, it will ask you to insert a servername and a serveralias that will be used to configure the virtualhost and create custom domain directories

Based on geerlingguy/ansible-role-apache
